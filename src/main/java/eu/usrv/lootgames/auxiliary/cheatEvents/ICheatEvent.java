
package eu.usrv.lootgames.auxiliary.cheatEvents;


import net.minecraft.entity.player.EntityPlayer;


public interface ICheatEvent
{
  void doEvent( EntityPlayer pPlayer );
}
