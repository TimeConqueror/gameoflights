package eu.usrv.lootgames.chess;


import com.jamesswafford.chess4j.exceptions.IllegalMoveException;
import com.jamesswafford.chess4j.exceptions.ParseException;
import com.jamesswafford.chess4j.io.InputParser;
import eu.usrv.lootgames.LootGames;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ChessEngineProxy
{
  private static ChessEngineProxy _mInstance;
  private UUID _mCurrentAttachedChessGame = null;
  private List<IChessEventListener> listeners = new ArrayList<IChessEventListener>();

  public static ChessEngineProxy getInstance()
  {
    if( _mInstance == null )
      _mInstance = new ChessEngineProxy();

    return _mInstance;
  }

  private ChessEngineProxy()
  {
  }

  public UUID getEngineToken()
  {
    if( _mCurrentAttachedChessGame != null )
      return null;
    else
      _mCurrentAttachedChessGame = UUID.randomUUID();

    return _mCurrentAttachedChessGame;
  }

  public void resetEngine()
  {
    sendCommand( "force" );
    sendCommand( "new" );
    sendCommand( "level 10 99 0" );
    sendCommand( "easy" );
  }

  private boolean sendCommand( String pCommand )
  {
    try
    {
      InputParser.getInstance().parseCommand( pCommand );
    }
    catch( IllegalMoveException ime )
    {
      LootGames.mLog.error( "Illegal move" );
    }
    catch( ParseException pe )
    {
      LootGames.mLog.error( "Parse error: " + pe.getMessage() );
    }
    catch( Exception e )
    {
      LootGames.mLog.debug( "Caught (hopefully recoverable) exception: " + e.getMessage() );
    }
    return true;
  }

  public void addListener( IChessEventListener toAdd )
  {
    listeners.add( toAdd );
  }

  public void publishAnswer( String pMessage )
  {
    for( IChessEventListener hl : listeners )
      hl.chessEngineMessage( _mCurrentAttachedChessGame, pMessage );
  }
}


interface IChessEventListener
{
  void chessEngineMessage( UUID pUUID, String pMessage );
}