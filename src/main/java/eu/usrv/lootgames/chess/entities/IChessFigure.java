
package eu.usrv.lootgames.chess.entities;


public interface IChessFigure
{
  public FiguresData getFiguresData();
}
